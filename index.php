</<!DOCTYPE html>

<html>
<head>
  <meta charset="utf-8"/>
  <meta title="2048" />
  <link rel="stylesheet" type="text/css" href="css/style.css">
  <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
  <script src="js/script.js"></script>
</head>
<body>
  <div class='main-area'>
    <div class='board'>
      <?php for($i = 0; $i < 4; $i++){
              echo "<div class='row'>";
          for($j = 0; $j < 4; $j++){
                    $num = ($i*3)+$j+1;
                    echo "<div id='".$num."' class='cell'><div class='item'></div></div>";
                    echo "";
          }
              echo "</div>";
      } ?>
    </div>
    <div></div>
  </div>
</body>
</html>
